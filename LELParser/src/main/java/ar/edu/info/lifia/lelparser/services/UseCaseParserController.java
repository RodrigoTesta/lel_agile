package ar.edu.info.lifia.lelparser.services;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import lifia.lelparser.exceptions.InvalidRequirementFormatException;
import lifia.lelparser.model.NLParser;
import lifia.lelparser.model.UserStory;
import lifia.lelparser.model.parserResults.ParserResult;

@RestController
public class UseCaseParserController {
static final String EXAMPLE_REQUEST="[{"
		+ "\"id\": \"string\","+
" \"text\": \"As a moderator I want to create a new game by entering a name and an optional description So that I can start inviting estimators\"";

	@RequestMapping(value="/parser", method = RequestMethod.POST)
	
	public ParserResult parseUserStory(@ApiParam(defaultValue=EXAMPLE_REQUEST) @RequestBody List<UserStory> userStoryList) throws InvalidRequirementFormatException {
		
		NLParser parser = new NLParser();

		// Parse to map (sample phrase)
		ParserResult result = parser.parseBatchUserStories(userStoryList);

		return result;
	}
}