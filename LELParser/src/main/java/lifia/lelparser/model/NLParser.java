package lifia.lelparser.model;

import lifia.lelparser.model.parserResults.ParserResult;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import java.io.StringReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lifia.lelparser.exceptions.InvalidRequirementFormatException;

/**
 *
 * @author Juan Ignacio Tonelli <jtonelli@lifia.info.unlp.edu.ar>
 */
public class NLParser {

    /**
     * Parser model (language)
     */
    private String parserModel;

    /**
     * Lexicalized parser
     */
    private LexicalizedParser lexicalizedParser;

    /**
     * Default constructor.
     */
    public NLParser() {
        // By default, the parser model is set to English
        this.parserModel = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";

        // Load the default lexicalized parser with the parser model
        lexicalizedParser = LexicalizedParser.loadModel(parserModel);
    }

    /**
     * @return the parserModel
     */
    public String getParserModel() {
        return parserModel;
    }

    /**
     * @param parserModel the parserModel to set
     */
    public void setParserModel(String parserModel) {
        this.parserModel = parserModel;
    }

    /**
     * @return the localizedParser
     */
    public LexicalizedParser getLexicalizedParser() {
        return lexicalizedParser;
    }

    /**
     * @param lexicalizedParser the localizedParser to set
     */
    public void setLexicalizedParser(LexicalizedParser lexicalizedParser) {
        this.lexicalizedParser = lexicalizedParser;
    }

    /**
     * Parse a given string with the configured model and parser
     *
     * @param textToBeParsed the String to be parsed
     * @return a String with the parsed result
     */
    public String parseToRawString(String textToBeParsed) {

        // Convert to lower case
        textToBeParsed = textToBeParsed.toLowerCase();

        TokenizerFactory<CoreLabel> tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
        Tokenizer<CoreLabel> tok = tokenizerFactory.getTokenizer(new StringReader(textToBeParsed));
        List<CoreLabel> rawWords2 = tok.tokenize();
        Tree parse = this.getLexicalizedParser().apply(rawWords2);

        TreebankLanguagePack tlp = this.getLexicalizedParser().treebankLanguagePack(); // PennTreebankLanguagePack for English
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
        GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
        List<TypedDependency> tdl = gs.typedDependenciesCCprocessed();

        // Return the parsed string
        return parse.pennString();
    }

        /**
     * Parse a given string with the configured model and parser
     *
     * @param usToBeParsed the User Story to be parsed
     * @return an array of Strings containing [1]
     * @throws lifia.sparser.exceptions.InvalidRequirementFormatException
     */
    public Map<String, PhraseStructure> parseToMap(UserStory usToBeParsed) throws InvalidRequirementFormatException {
        // Result array
        Map<String, PhraseStructure> result = new HashMap<>();


        // First part ("AS")
        this.parseContents(result, usToBeParsed.getAsSentence().toLowerCase(), false, usToBeParsed);

        // Second part ("I WANT TO")
        this.parseContents(result, usToBeParsed.getIWantToSentence().toLowerCase(), true, usToBeParsed);

        // Third part ("SO THAT")
        if (usToBeParsed.hasSoThatSection())
        	this.parseContents(result, usToBeParsed.getSoThatSentence().toLowerCase(), false, usToBeParsed);

        // Return the resulting array
        return result;
    }
    
    /**
     * Parses a collection of user stories.
     * @param userStories the user stories to parse.
     * @return the resulting data of the parsing.
     */
    public ParserResult parseBatchUserStories (Collection<UserStory> userStories) throws InvalidRequirementFormatException
    {
        ParserResult result = new ParserResult();
        result.setUserStories(userStories);
        
        for (UserStory currentUS : userStories)
        {
        	try{
            Map<String, PhraseStructure> currUSResult = this.parseToMap(currentUS);
            for (PhraseStructure currST : currUSResult.values())
            {
                result.addContents(currST.getSubjects());
                result.addContents(currST.getObjects());
                result.addContents(currST.getVerbs());
            }
        	}catch (Exception e) {
				System.out.println("There is a problem with the following US:"+currentUS.getText());
			}
        }
        
        return result;
        
    }



    /**
     * Parse the contents of a single phrase
     *
     * @param phrase
     */
    private void parseContents(Map<String, PhraseStructure> contentMap, String phrase, boolean treatAsNotionSection, UserStory usToBeParsed) {

        // The contents of the phrase
        PhraseStructure pContents = new PhraseStructure();

        // Preparse the phrase
        String ppPhrase = this.parseToRawString(phrase);

        // Parse subjects
        this.parseSubjects(ppPhrase, pContents, treatAsNotionSection, usToBeParsed);

        // Parse actions
        this.parseVerbs(ppPhrase, pContents, usToBeParsed);

        // Put the parsed contents into the map
        contentMap.put(phrase, pContents);
    }

    protected void parseSubjects(String processedPhrase, PhraseStructure pContents, boolean treatAsObjects, UserStory usToBeParsed) {
        int nnStartIndex = processedPhrase.indexOf("(NN ");
        int nnEndIndex = processedPhrase.indexOf(")", nnStartIndex);

        // While exists a noun...
        while (nnStartIndex != -1) {
            if (treatAsObjects)
            {
                pContents.addObject(processedPhrase.substring(nnStartIndex + 4, nnEndIndex), usToBeParsed);    
            }
            else
            {
                pContents.addSubject(processedPhrase.substring(nnStartIndex + 4, nnEndIndex), usToBeParsed);    
            }            
            nnStartIndex = processedPhrase.indexOf("(NN ", nnEndIndex);
            nnEndIndex = processedPhrase.indexOf(")", nnStartIndex);
        }

    }

    protected void parseVerbs(String phrase, PhraseStructure pContents, UserStory usToBeParsed) {
        int vbStartIndex = phrase.indexOf("(VB ");
        int vbEndIndex = phrase.indexOf(")", vbStartIndex);

        // While exists a noun...
        while (vbStartIndex != -1) {
            pContents.addVerb(phrase.substring(vbStartIndex + 4, vbEndIndex), usToBeParsed);
            vbStartIndex = phrase.indexOf("(VB ", vbEndIndex);
            vbEndIndex = phrase.indexOf(")", vbStartIndex);
        }
    }

}
