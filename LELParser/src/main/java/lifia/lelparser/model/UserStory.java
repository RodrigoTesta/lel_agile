package lifia.lelparser.model;

import org.assertj.core.groups.Tuple;

import edu.stanford.nlp.util.Pair;
import lifia.lelparser.exceptions.InvalidRequirementFormatException;

/**
 *
 * @author Juan Ignacio Tonelli <jtonelli@lifia.info.unlp.edu.ar>
 */
public class UserStory {

	private static final String[] SO_THAT = new String[]{"so that", "that is"};
	private static final String[] I_WANT_TO = new String[] { "i want to","i want", "," };
	private static final String AS = "as";
	private String id;
	private String text;

	public UserStory() {

	}

	public UserStory(String id, String text) {
		this.id = id;
		this.text = text;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	public String getAsSentence() {
		String us = this.getText().toLowerCase();
		int indexAS = us.indexOf(AS);

		Pair<String, Integer> iwtPair = lookup(us, I_WANT_TO);

		String result = us.substring(indexAS + AS.length() + 1, (iwtPair != null) ? iwtPair.second(): text.length());
		return result;

	}

	public String getIWantToSentence() {
		String string = this.getText().toLowerCase();
		
		
		
		Pair<String, Integer> iwtPair=lookup(string, I_WANT_TO);
		Pair<String, Integer> stPair=lookup(string,SO_THAT);
        
		String result = string.substring(iwtPair.second+ iwtPair.first().length()+1, (stPair!=null)?stPair.second:text.length());
		return result;
        
	}

	public String getSoThatSentence() {
		String string = this.getText().toLowerCase();

		Pair<String, Integer> stPair=lookup(string,SO_THAT);
		
		String result= string.substring(stPair.second + stPair.first.length() + 1, text.length());
		return result;

	}

	

	public boolean hasSoThatSection() {
		return lookup(text,SO_THAT)!=null;
	}

	private Pair<String, Integer> lookup(String body, String[] param) {
		for (String string : param) {
			if (body.toLowerCase().contains(string.toLowerCase())) {
				return new Pair<>(string,body.toLowerCase().indexOf(string.toLowerCase()));
			}
		}
		return null;
	}

}
