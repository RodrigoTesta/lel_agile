# README #

Use Case processor for deriving LEL elements


### How do I get set up? ###

* Requirements

It requires Maven and Java SDK installed.

* Summary of set up


```
#!Bash

git clone https://bitbucket.org/lellifia/lel_agile.git

cd lel_agile

mvn spring-boot:run
```


* Database configuration
* How to run tests

The testing is quite strightforward, you need to open a browser pointing out to: [localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html#!/use-case-parser-controller/) and then you must introduce following input to the use-case-parser-*controller>> **/parse*** operation:

```
#!json

[
  {
    "id": "string",
    "text": "As a moderator I want to create a new game by entering a name and an optional description So that I can start inviting estimators"
  }
]
```